from flask import Flask, render_template, json, url_for, jsonify 
app = Flask(__name__)

with open('static/carlist.json') as jsonfile:
  dict = json.load(jsonfile)
  jsonfile.close()
  Data = {
    'dict' : dict
  }

@app.route('//')
def index():
  return render_template('index.html', methods=['GET'])

@app.route('/about/', methods=['GET'])
def about():
  return render_template('about.html')

@app.route('/inventory/', methods=['GET'])
def inventory():
    return render_template('inventory.html', **Data)

@app.route('/inventory/porsche-911-gt3/', methods=['GET'])
def porsche():
    return render_template('gt3.html', **Data)

@app.route('/inventory/bmw-m3/', methods=['GET'])
def bmw():
    return render_template('m3.html', **Data)

@app.route('/inventory/audi-rs6/', methods=['GET'])
def audi():
    return render_template('rs6.html', **Data)

@app.route('/inventory/mercedes-benz-amg-gt-gts/', methods=['GET'])
def mercedes():
    return render_template('amg.html', **Data)

@app.route('/contact/', methods=['GET'])
def contact():
  return render_template('contact.html')

if __name__ == "__main__":
  app.run(host='0.0.0.0', debug=True)
