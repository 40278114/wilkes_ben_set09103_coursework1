$(document).ready(function () {
$('#AudiCheck').attr("checked",true);
$('#BMWCheck').attr("checked",true);
$('#PorscheCheck').attr("checked",true);
$('#MercCheck').attr("checked",true);

  $('#AudiCheck').change(function () {
    if (!this.checked){ 
      $('#RS6').hide();
    }
    if (this.checked){
      $('#RS6').show();
    }
  });

  $('#BMWCheck').change(function () {
    if (!this.checked){ 
      $('#M3').hide();
    }
    if (this.checked){
      $('#M3').show();
    }
  });

  $('#PorscheCheck').change(function () {
    if (!this.checked){ 
      $('#911').hide();
    }
    if (this.checked){
      $('#911').show();
    }
  });

  $('#MercCheck').change(function () {
    if (!this.checked){ 
      $('#AMG').hide();
    }
    if (this.checked){
      $('#AMG').show();
    }
  });
});
